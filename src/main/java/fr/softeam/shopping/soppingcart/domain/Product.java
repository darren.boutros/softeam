package fr.softeam.shopping.soppingcart.domain;

import org.apache.commons.lang3.StringUtils;

public class Product {
	
	private String name;
	private double price;
	
	public Product(String name, double price) {
		super();
		this.setName(name);
		this.setPrice(price);
	}
	
	public void setName(String name) {
		if (StringUtils.isBlank(name)) throw new RuntimeException ("the product name must not be empty !!!");
		
		this.name = name;
	}
	public void setPrice(double price) {
		if (price < 0.0) throw new RuntimeException ("the product price must not be negative !!!");
		this.price = price;
	}

	public double getPrice() {
		return price;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "name=" + name + ", price=" + price;
	}
	
	

}
