package fr.softeam.shopping.soppingcart.domain;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ShoppingCart {
	
	Map<Product, Integer> items;

	
	public ShoppingCart() {
		super();
		items = new ConcurrentHashMap<Product, Integer>();
	}

	//both solutions are correct :)
	public Double getTotal() {
		
		return items.entrySet().stream()
				.map(e -> e.getKey().getPrice() * e.getValue())
				.reduce(0.0, (sum, n) -> sum +n );

//	     return items.entrySet().stream()
//		    		.mapToDouble(e -> e.getKey().getPrice() * e.getValue())
//		    		.sum();
	}

	public void add(Product p) {
		items.put(p, items.containsKey(p)? items.get(p)+1 : 1);
	}

	public void remove(Product p) {
		if (!items.containsKey(p)) throw new RuntimeException ("Product not found");
		
		if (items.get(p)==1) 
			items.remove(p);
		else
			items.put(p, items.get(p)-1);
	}
	public Map<Product, Integer> getItems() {
		return items;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ShoppingCart : \n");
		stringBuilder.append(this.printItems());
		stringBuilder.append("\t---------------------------------\n");
		stringBuilder.append("\t \t \t Total :"+ String.format("%5.2f",  this.getTotal()));
		return  stringBuilder.toString();
	}
	
	private String printItems() {
		StringBuilder stringBuilder = new StringBuilder();
		this.items.forEach((k, v) -> {
			stringBuilder.append(String.format("%8s price %4.2f no %d for total %5.2f", k.getName(), k.getPrice(), v, k.getPrice()*v)); 
			stringBuilder.append("\n");
		});
		return stringBuilder.toString();
	}
	
	
}
