package fr.softeam.shopping.soppingcart;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.Before;
import org.junit.Test;

import fr.softeam.shopping.soppingcart.domain.Product;
import fr.softeam.shopping.soppingcart.domain.ShoppingCart;

public class ShoppingCartTest {

	private ShoppingCart shoppingCart;
	private Product p1, p2, p3;

	@Before
	public void setUp() {
		shoppingCart = new ShoppingCart();
		p1 = new Product("Apple", 400.0);
		p2 = new Product("Microsoft", 500.0);
		p3 = new Product("Dell", 600.0);
	}

	@Test
	public void verifyEmptyCart() {
		assertThat(shoppingCart.getTotal()).isEqualTo(0.0);
	}

	@Test 
	public void verifyOnceItem() {
		shoppingCart.add(p1);
		assertThat(shoppingCart.getItems().size()).isEqualTo(1);
		assertThat(shoppingCart.getTotal()).isEqualTo(p1.getPrice());
	}

	@Test 
	public void verifyTwoDiffrentItem() {
		shoppingCart.add(p1);
		shoppingCart.add(p2);
		assertThat(shoppingCart.getItems().size()).isEqualTo(2);
		assertThat(shoppingCart.getTotal()).isEqualTo(p1.getPrice()+p2.getPrice());
	}

	@Test 
	public void verifySametItemTwice() {
		shoppingCart.add(p1);
		shoppingCart.add(p1);
		assertThat(shoppingCart.getItems().size()).isEqualTo(1);
		assertThat(shoppingCart.getItems().get(p1)).isEqualTo(2);
		assertThat(shoppingCart.getTotal()).isEqualTo(p1.getPrice()+p1.getPrice());
	}

	@Test
	public void removeProduct() {
		shoppingCart.add(p3);
		shoppingCart.remove(p3);
		assertThat(shoppingCart.getItems().size()).isEqualTo(0);
		assertThat(shoppingCart.getTotal()).isEqualTo(0.0);
	}

	@Test
	public void removeProductNotInTheCart() {
		assertThatExceptionOfType(RuntimeException.class )
		.isThrownBy(()->shoppingCart.remove(p3) )
		.withMessage("Product not found")
		.withNoCause();

	}

	@Test
	public void general() {
		shoppingCart.add(p1);
		shoppingCart.add(p2);
		shoppingCart.remove(p2);
		shoppingCart.add(p3);
		assertThat(shoppingCart.getItems().size()).isEqualTo(2);
		assertThat(shoppingCart.getTotal()).isEqualTo(1000.0);
	}
	@Test
	public void show() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ShoppingCart : \n");
		stringBuilder.append(String.format("%8s price %4.2f no %d for total %5.2f", "Dell", 600.0, 1, 600.0));
		stringBuilder.append(String.format("%8s price %4.2f no %d for total %5.2f", "Apple", 400.0, 1, 400.0)); 
		stringBuilder.append("\n");
		stringBuilder.append("\t---------------------------------\n");
		stringBuilder.append("\t \t \t Total :"+ String.format("%5.2f",  1000.00));

		shoppingCart.add(p1);
		shoppingCart.add(p2);
		shoppingCart.remove(p2);
		shoppingCart.add(p3);
		
		assertThat( shoppingCart.toString().equalsIgnoreCase(stringBuilder.toString()));
	}
}
